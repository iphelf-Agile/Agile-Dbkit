#include "c_wrapper.h"
#include "Cluster/Evaluate/evaluate.h"
#include "Cluster/Ckmeans.1d.dp/Ckmeans.1d.dp.h"
#include <QByteArray>
#include <algorithm>
using namespace std;


Dbkit *dbkit=nullptr;

int dbkit_new() {
    //printf("dbkit_new\n");
    dbkit=new Dbkit();
    return true;
}

int dbkit_delete() {
    //printf("dbkit_delete\n");
    if(dbkit==nullptr) return false;
    dbkit_disconnect();
    delete dbkit;
    dbkit=nullptr;
    return 1;
}

int dbkit_execute(const char *command) {
    //printf("dbkit_execute\n");
    return dbkit->execute(command);
}

int dbkit_executeScript(const char *script) {
    //printf("dbkit_executeScript\n");
    return dbkit->executeScript(script);
}

int dbkit_connect(const char *databasePath) {
    //printf("dbkit_connect\n");
    return dbkit->connect(databasePath);
}

int dbkit_disconnect() {
    //printf("dbkit_disconnect\n");
    return dbkit->disconnect();
}

int dbkit_setKey(const char *key) {
    //printf("dbkit_setKey\n");
    return dbkit->setKey(key);
}

int dbkit_setMode(const char *mode) {
    //printf("dbkit_setMode\n");
    return dbkit->setMode(mode);
}

int dbkit_setEvaluation(const char *evaluation) {
    //printf("dbkit_setEvaluation\n");
    return dbkit->setEvaluation(evaluation);
}

int dbkit_setCheckpoint(const char *checkpoint) {
    //printf("dbkit_setCheckpoint\n");
    return dbkit->setCheckpoint(checkpoint);
}

const int MAXB=1e7;
char buffer[MAXB];
int json_wrapper(const char *input, char **output,
                 bool (Dbkit::*json_op)(QString &,QString)) {
    //printf("json_wrapper\n");
    QString s;
    bool r=(dbkit->*json_op)(s,input);
    QByteArray a=s.toUtf8();
    int size=min(MAXB,a.size());
    memcpy(buffer,a,size);
    buffer[size]='\0';
    *output=buffer;
    return r;
}

int dbkit_json_insertRows(const char *input, char **output) {
    //printf("dbkit_json_insertRows\n");
    return json_wrapper(input,output,&Dbkit::json_insertRows);
}

int dbkit_json_selectRows(const char *input, char **output) {
    //printf("dbkit_json_selectRows\n");
    return json_wrapper(input,output,&Dbkit::json_selectRows);
}

int dbkit_json_selectJoinedRow(const char *input, char **output) {
    //printf("dbkit_json_selectJoinedRow\n");
    return json_wrapper(input,output,&Dbkit::json_selectJoinedRow);
}

int dbkit_json_updateRows(const char *input,char **output) {
    //printf("dbkit_json_updateRows\n");
    return json_wrapper(input,output,&Dbkit::json_updateRows);
}

int dbkit_json_deleteRows(const char *input,char **output) {
    //printf("dbkit_json_deleteRows\n");
    return json_wrapper(input,output,&Dbkit::json_deleteRows);
}

int dbkit_json_getTables(const char *input, char **output) {
    //printf("dbkit_json_getTables\n");
    return json_wrapper(input,output,&Dbkit::json_getTables);
}

int dbkit_json_getColumns(const char *input, char **output) {
    //printf("dbkit_json_getColumns\n");
    return json_wrapper(input,output,&Dbkit::json_getColumns);
}

int dbkit_json_count(const char *input, char **output) {
    //printf("dbkit_json_count\n");
    return json_wrapper(input,output,&Dbkit::json_count);
}

int dbkit_json_sum(const char *input, char **output) {
    //printf("dbkit_json_sum\n");
    return json_wrapper(input,output,&Dbkit::json_sum);
}
