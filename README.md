### How to run?

Use `./dbkit_wrapper.py -h` to check it out yourself, or:

```Shell
./dbkit_wrapper.py -m train -d company.db -c company.checkpoint -vs company.script
```

or:

```DOS
python3 dbkit_wrapper.py -m train -d company.db -c company.checkpoint -vs company.script
```

### Performance

10s on my laptop.
