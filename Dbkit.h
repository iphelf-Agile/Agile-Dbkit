#ifndef DBKIT_H
#define DBKIT_H

#include <QStringList>
#include "Database/Database.h"

class Dbkit {
  public:
    enum class State {Disconnected,Connected};
    Dbkit();
    ~Dbkit();
    bool execute(QString command);
    bool executeScript(QString script);
    bool connect(QString databaseName);
    bool disconnect();
    bool setKey(QString key);
    bool setMode(QString mode);
    bool setEvaluation(QString evaluation=QString());
    bool setCheckpoint(QString checkpoint=QString());
    bool copy(QString dst,QString src,QStringList columns=QStringList());
    bool cluster(QString dst, QString src, QString weight=QString());
    bool rate(QString dst,QString src,QString criteria=QString(),QString target=QString());
    bool count(QString dst,QString src);
    bool load(QStringList tables);
    bool save(QStringList tables);
    bool drop(QStringList tables);
    bool rename(QString dst,QString src);
    bool clone(QString dst,QString src);
    bool json_insertRows(QString &output,QString input);
    bool json_deleteRows(QString &output,QString input);
    bool json_updateRows(QString &output,QString input);
    bool json_selectRows(QString &output,QString input);
    bool json_selectJoinedRow(QString &output,QString input);
    bool json_getTables(QString &output,QString input);
    bool json_getColumns(QString &output,QString input);
    bool json_count(QString &output,QString input);
    bool json_sum(QString &output,QString input);
    void json_pack(QString &json,QString &code,QJsonObject &data);
    void json_unpack(QString &code,QJsonObject &data,QString &json);
    bool interpretName(
        QString &target,QString &type,QString &arg,QString &mode,
        QString name
    );
    bool interpretCommand(
        QString &commandName, QStringList &commandArgumentList,
        QString command
    );
    Table &createTable(QString tableName, QString keyName);
    void copyIds(Table &dst,Table &src);

    State state;
    QString key;
    QString mode;
    QString tableEvaluation;
    QString pathCheckpoint;
    QStringList checkpointCache;

    Driver *driver;
    Database db;
};

#if defined(DBKIT_LIBRARY)
#  define DBKIT_EXPORT Q_DECL_EXPORT
#else
#  define DBKIT_EXPORT Q_DECL_IMPORT
#endif

#define DBKIT_EXPORT_C extern "C" DBKIT_EXPORT

DBKIT_EXPORT_C
void Ckmeans_1d_dp(
    double* x, int n, int minK, int maxK, double* w,
    int* y, double* centers, double* size, double* withinss, double* BICs
);

DBKIT_EXPORT_C
void evaluate(
    double* x, int* y, int n, int nCluster, double* centers, double* size,
    int* nScore, double** score, char*** method
);

#endif // DBKIT_H
