from dbkit import Dbkit
import json


def encode(object):
    return json.dumps(object, ensure_ascii=False, indent=True)


def main():
    dbkit = Dbkit()
    dbkit.connect("test.db")
    dbkit.execute("load")

    # jsonGetTables
    #  input{
    # 		code: <code> (0)
    # 		data: {}
    # 	}
    # 	output{
    # 		code: <code> (S0)
    # 		data(S0): {
    # 			tables: [ <table>,... ]
    # 		}
    # 	}
    jsonGetTables = {
        "code": "0",
        "data": {}
    }
    print("jsonGetTables" + (" *" * 30))
    print("input", end="")
    print(encode(jsonGetTables))
    print("output", end="")
    print(encode(dbkit.jsonGetTables(jsonGetTables)[1]))

    # jsonGetColumns
    #  input{
    # 		code: <code> (0)
    # 		data: {
    # 			table: <table>
    # 		}
    # 	}
    # 	output{
    # 		code: <code> (S0,F1:no table)
    # 		data(S0): {
    # 			columns: [ <column>,... ]
    # 			types: [ <types>,... ]
    # 		}
    # 	}
    jsonGetColumns = {
        "code": "0",
        "data": {
            "table": "账户"
        }
    }
    print("jsonGetColumns" + (" *" * 30))
    print("input", end="")
    print(encode(jsonGetColumns))
    print("output", end="")
    print(encode(dbkit.jsonGetColumns(jsonGetColumns)[1]))

    # jsonInsertRows
    #  input{
    # 		code: <code> (0)
    # 		data: {
    # 			table: <table>
    # 			columns: [ <column>,... ]
    #  		rows: [ [ <value>,... ],... ]
    # 		}
    # 	}
    # 	output{
    # 		code: <code> (S0,F1:no table,F2:no column,F3:row format,F4:conflicted)
    # 		data(F1): {}
    # 		data(F2): {
    # 			columns: [ <column>,... ]
    # 		}
    # 		data(F3): {
    # 			rows: [ <#row>,... ]
    # 		}
    # 		data(F4): {
    # 			rows: [ <#row>,... ]
    # 		}
    #  }
    jsonInsertRows = {
        "code": "0",
        "data": {
            "table": "账户",
            "columns": ["用户名", "密码"],
            "rows": [
                ["user1", "password1"],
                ["user2", "password2"]
            ]
        }
    }
    print("jsonInsertRows" + (" *" * 30))
    print("input", end="")
    print(encode(jsonInsertRows))
    print("output", end="")
    print(encode(dbkit.jsonInsertRows(jsonInsertRows)[1]))

    # jsonCount
    # 	input{
    # 		code: <code> (0)
    # 		data:{
    # 			table: <table>
    # 			column: <column>
    # 		}
    # 	}
    # 	output{
    # 		code: <code> (S0,F1:no table,F2:no column)
    # 		data(S0):{
    # 			count: { <value>:<count>,... }
    # 		}
    # 		data(F1):{}
    # 		data(F2):{}
    #  }
    jsonCount = {
        "code": "0",
        "data": {
            "table": "账户",
            "column": "管理员"
        }
    }
    print("jsonCount" + (" *" * 30))
    print("input", end="")
    print(encode(jsonCount))
    print("output", end="")
    print(encode(dbkit.jsonCount(jsonCount)[1]))

    # jsonSum
    # 	input{
    # 		code: <code> (0)
    # 		data:{
    # 			table: <table>
    # 			column: <column>
    # 		}
    # 	}
    # 	output{
    # 		code: <code> (S0,F1:no table,F2:no column)
    # 		data(S0):{
    # 			count: { <value>:<count>,... }
    # 		}
    # 		data(F1):{}
    # 		data(F2):{}
    #  }
    jsonSum = {
        "code": "0",
        "data": {
            "table": "账户",
            "column": "管理员"
        }
    }
    print("jsonSum" + (" *" * 30))
    print("input", end="")
    print(encode(jsonSum))
    print("output", end="")
    print(encode(dbkit.jsonSum(jsonSum)[1]))

    # jsonUpdateRows
    #  input{
    # 		code: <code> (0)
    # 		data: {
    # 			table: <table>
    # 			columns: [ <column>,... ]
    # 			rows: [ [ <value>,... ],... ]
    # 		}
    # 	}
    # 	output{
    # 		code: <code> (S0,F1:no table,F2:no column,F3:row format,F4:no row)
    # 		data(F1): {}
    # 		data(F2): {
    # 			columns: [ <column>,... ]
    # 		}
    # 		data(F3): {
    # 			rows: [ <#row>,... ]
    # 		}
    # 		data(F4): {
    # 			rows: [ <#row>,... ]
    # 		}
    #  }
    jsonUpdateRows = {
        "code": "0",
        "data": {
            "table": "账户",
            "columns": ["用户名", "密码"],
            "rows": [
                ["user1", "newPassword"]
            ]
        }
    }
    print("jsonUpdateRows" + (" *" * 30))
    print("input", end="")
    print(encode(jsonUpdateRows))
    print("output", end="")
    print(encode(dbkit.jsonUpdateRows(jsonUpdateRows)[1]))

    # json_selectRows
    #  input{
    # 		code: <code> (0)
    # 		data: {
    # 			tables: [ <table>,... ]
    # 			columns: [ <column>,... ]
    # 			ids: [ <id>,... ]
    # 			constraints: {
    # 				<column>: [ <value>,... ],
    # 				...
    # 			}
    # 			orderby: [ <column>,... ]
    # 			ascending: [ <ascending>,... ]
    # 			limit: <limit>
    # 			page: <page>(1-based)
    # 		}
    # 	}
    # 	output{
    # 		code: <code> (S0,F1:no table,F2:no column,F3:no id,F4:invalid page)
    # 		data(S0): {
    # 			current: <iPage>
    # 			total: <nPage>
    # 			rows: [ [ <value>,... ],... ]
    # 		}
    # 		data(F1): {}
    # 		data(F2): {
    # 			columns: [ <column>,... ]
    # 		}
    # 		data(F3): {
    # 			ids: [ <#id>,... ]
    # 		}
    # 		data(F4): {}
    #  }
    jsonSelectRows1 = {
        "code": "0",
        "data": {
            "table": "账户",
            "columns": ["用户名", "密码", "管理员"],
            "ids": ["agile", "user1"],
            "constraints": {},
            "orderby": [],
            "limit": 30,
            "page": 1
        }
    }
    print("jsonSelectRows1" + (" *" * 30))
    print("input", end="")
    print(encode(jsonSelectRows1))
    print("output", end="")
    print(encode(dbkit.jsonSelectRows(jsonSelectRows1)[1]))

    # jsonDeleteRows
    #  input{
    # 		code: <code> (0)
    # 		data: {
    # 			table: <table>
    # 			ids: [ <id>,... ]
    # 		}
    # 	}
    # 	output{
    # 		code: <code> (S0,F1:no table,F2:no id)
    # 		data(F1): {}
    # 		data(F2): {
    # 			ids: [ <#id>,... ]
    # 		}
    #  }
    jsonDeleteRows = {
        "code": "0",
        "data": {
            "table": "账户",
            "ids": ["user1", "user2"]
        }
    }
    print("jsonDeleteRows" + (" *" * 30))
    print("input", end="")
    print(encode(jsonDeleteRows))
    print("output", end="")
    print(encode(dbkit.jsonDeleteRows(jsonDeleteRows)[1]))

    jsonSelectRows2 = {
        "code": "0",
        "data": {
            "table": "账户",
            "columns": ["用户名", "密码", "管理员"],
            "ids": [],
            "constraints": {},
            "orderby": ["用户名"],
            "ascending": [False],
            "limit": 1,
            "page": 1
        }
    }
    print("jsonSelectRows2" + (" *" * 30))
    print("input", end="")
    print(encode(jsonSelectRows2))
    print("output", end="")
    print(encode(dbkit.jsonSelectRows(jsonSelectRows2)[1]))

    # jsonSelectJoinedRow
    #  input{
    # 		code: <code> (0)
    # 		data: {
    # 			tables: [ <table>,... ]
    # 			columns: [ [ <column>,... ],... ]
    # 			id: <id>
    # 		}
    # 	}
    # 	output{
    # 		code: <code> (S0,F1:no table,F2:column format,F3:no column,F4:no id)
    # 		data(S0): {
    # 			row: [ <value>,... ]
    # 		}
    # 		data(F1): {}
    # 		data(F2): {
    # 			tables: [ <table>,... ]
    # 		}
    # 		data(F3): {
    # 			columns: [ [<column>,...],... ]
    # 		}
    # 		data(F4): {}
    #  }
    jsonSelectJoinedRow = {
        "code": "0",
        "data": {
            "tables": ["账户", "个人信息"],
            "columns": [
                ["用户名", "管理员"],
                ["昵称", "性别"]
            ],
            "id": "guest"
        }
    }
    print("jsonSelectJoinedRow" + (" *" * 30))
    print("input", end="")
    print(encode(jsonSelectJoinedRow))
    print("output", end="")
    print(encode(dbkit.jsonSelectJoinedRow(jsonSelectJoinedRow)[1]))


if __name__ == '__main__':
    main()
