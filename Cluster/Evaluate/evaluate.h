#ifndef EVALUATE_H
#define EVALUATE_H

void GetEvaluation(
    double *x, int *y, int n, int nCluster, double *centers, double *size,
    int *nScore, double **pScore, char ***pMethod);

#endif // EVALUATE_H
