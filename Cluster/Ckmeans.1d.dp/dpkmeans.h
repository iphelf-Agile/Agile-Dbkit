#ifndef DPKMEANS_H
#define DPKMEANS_H

#include <string>
#include "Ckmeans.1d.dp.h"

void dpKmeans(
    double *x, int n, int minK, int maxK, double *w,
    int *y, double *centers, double *size, double *withinss, double *BICs){
    std::string estimate_k="BIC";
    std::string method="linear";
    kmeans_1d_dp(x,n,w,minK,maxK,
                 y,centers,withinss,size,BICs,
                 estimate_k,method,L2);
}

#endif // DPKMEANS_H
