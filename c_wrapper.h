#ifndef C_WRAPPER_H
#define C_WRAPPER_H

#include <QtCore/qglobal.h>
#include "Dbkit.h"

DBKIT_EXPORT_C
Dbkit *dbkit;

DBKIT_EXPORT_C
int dbkit_new();

DBKIT_EXPORT_C
int dbkit_delete();

DBKIT_EXPORT_C
int dbkit_execute(const char *command);

DBKIT_EXPORT_C
int dbkit_executeScript(const char *script);

DBKIT_EXPORT_C
int dbkit_connect(const char *databasePath);

DBKIT_EXPORT_C
int dbkit_disconnect();

DBKIT_EXPORT_C
int dbkit_setKey(const char *key);

DBKIT_EXPORT_C
int dbkit_setMode(const char *mode);

DBKIT_EXPORT_C
int dbkit_setEvaluation(const char *evaluation);

DBKIT_EXPORT_C
int dbkit_setCheckpoint(const char *checkpoint);

// Json related

DBKIT_EXPORT_C
int dbkit_json_insertRows(const char *input, char **output);

DBKIT_EXPORT_C
int dbkit_json_selectRows(const char *input,char **output);

DBKIT_EXPORT_C
int dbkit_json_selectJoinedRow(const char *input,char **output);

DBKIT_EXPORT_C
int dbkit_json_updateRows(const char *input, char **output);

DBKIT_EXPORT_C
int dbkit_json_deleteRows(const char *input, char **output);

DBKIT_EXPORT_C
int dbkit_json_getTables(const char *input,char **output);

//DBKIT_EXPORT_C
//int dbkit_json_deleteTables(const char *input,char **output);

DBKIT_EXPORT_C
int dbkit_json_getColumns(const char *input,char **output);

DBKIT_EXPORT_C
int dbkit_json_count(const char *input, char **output);

DBKIT_EXPORT_C
int dbkit_json_sum(const char *input, char **output);

#endif // C_WRAPPER_H
