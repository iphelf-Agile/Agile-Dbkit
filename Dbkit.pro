QT -= gui
QT += sql

CONFIG(release, debug|release) {
    TEMPLATE = lib
}

DEFINES += DBKIT_LIBRARY

CONFIG += c++11 console

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Cluster/Ckmeans.1d.dp/Ckmeans.1d.dp.cpp \
    Cluster/Ckmeans.1d.dp/EWL2_dynamic_prog.cpp \
    Cluster/Ckmeans.1d.dp/EWL2_fill_SMAWK.cpp \
    Cluster/Ckmeans.1d.dp/EWL2_fill_log_linear.cpp \
    Cluster/Ckmeans.1d.dp/EWL2_fill_quadratic.cpp \
    Cluster/Ckmeans.1d.dp/MCW_fill_SMAWK.cpp \
    Cluster/Ckmeans.1d.dp/MCW_functions.cpp \
    Cluster/Ckmeans.1d.dp/dynamic_prog.cpp \
    Cluster/Ckmeans.1d.dp/fill_SMAWK.cpp \
    Cluster/Ckmeans.1d.dp/fill_log_linear.cpp \
    Cluster/Ckmeans.1d.dp/fill_quadratic.cpp \
    Cluster/Ckmeans.1d.dp/select_levels.cpp \
    Cluster/Ckmeans.1d.dp/weighted_select_levels.cpp \
    Cluster/Evaluate/evaluate.cpp \
    Database/Database.cpp \
    Dbkit.cpp \
    c_wrapper.cpp \
    main.cpp

HEADERS += \
    Cluster/Ckmeans.1d.dp/Ckmeans.1d.dp.h \
    Cluster/Ckmeans.1d.dp/EWL2.h \
    Cluster/Ckmeans.1d.dp/EWL2_within_cluster.h \
    Cluster/Ckmeans.1d.dp/MCW_functions.h \
    Cluster/Ckmeans.1d.dp/dpkmeans.h \
    Cluster/Ckmeans.1d.dp/precision.h \
    Cluster/Ckmeans.1d.dp/within_cluster.h \
    Cluster/Evaluate/evaluate.h \
    Database/Database.h \
    Dbkit.h \
    c_wrapper.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    README.md \
    app.py \
    company.checkpoint \
    createTemp.script \
    dbkit.py \
    dbkit_old.py \
    dbkit_test.py \
    dbkit_test.py \
    scripts/createTemp.script \
    scripts/dropTemp.script \
    scripts/land.script \
    scripts/predict.script \
    scripts/takeOff.script \
    scripts/train.script \
    test.db

#INCLUDEPATH += D:/Programs/Eigen/headers
